# sync general pad
wget -O content/notes.md https://demo.codimd.org/vBfQqNJgT4a63dYxbCxSDg/download
# sync readme pad
wget -O README.md https://demo.codimd.org/gawCnlQyQla4ctWNLpkR9A/download
# sync presentation pad
wget -O content/presentation.md https://demo.codimd.org/GhjjMP9WRca71Pv8HgQ9iQ/download
# sync 1st session pad
wget -O content/post/session-01.md https://demo.codimd.org/BRExcT9kSxu2fuBJu7CaaQ/download
# sync 2nd session pad
wget -O content/post/session-02.md https://demo.codimd.org/laEmtIicR1CsGi6O5T0Tig/download
# sync 3d session pad
wget -O content/post/session-03.md https://demo.codimd.org/zLC2q0OvTAyB5haLXAIbkg/download
wget -O content/post/session-04.md https://demo.hedgedoc.org/P3otHIibSTKWkIuwltZmYw/download
wget -O content/post/ccla-conference.md https://demo.hedgedoc.org/bUzHlzPZQcKrIlHqBO3yYg/download
wget -O content/post/session-05.md https://demo.hedgedoc.org/tbj9SqEdQRS2zY3T9HyJng/download
wget -O content/post/session-06.md https://demo.hedgedoc.org/fJMQ4tcUQGiJEYCEchD93A/download
wget -O content/post/session-07.md https://demo.hedgedoc.org/eQ6fjB7WTzKEZxeRbqvSrw/download
wget -O content/post/session-08.md https://demo.hedgedoc.org/p62Zlsa6TN6FOseqRJcb9g/download
wget -O content/post/humanistica-working-group.md https://demo.hedgedoc.org/W5M4-OO9SemoBRYFAGhY5g/download
wget -O content/post/ccla-conference.md https://demo.hedgedoc.org/bUzHlzPZQcKrIlHqBO3yYg/download
wget -O content/ccla-conference/annual-meeting.md https://demo.hedgedoc.org/tl4XcSrdQNawTGMBMTvyuQ/download
# pandoc presentation.md --template html/template.html -o ./html/index.html
# pandoc notes.md --template html/template.html -o ./html/notes.html
# pandoc session1.md --template html/template.html -o ./html/session1.html
# pandoc session2.md --template html/template.html -o ./html/session2.html
# pandoc session3.md --template html/template.html -o ./html/session3.html
git status
git add content/post/session-01.md content/post/session-02.md content/post/session-03.md content/presentation.md content/notes.md content/post/session-04.md content/post/session-05.md content/post/session-06.md content/post/session-07.md content/post/session-08.md content/post/ccla-conference.md content/ccla-conference/annual-meeting.md content/post/humanistica-working-group.md
git commit -m "a new version!"
