---
title: Association Canadienne de Litérature Comparée/ Canadian Comparative Literature Association Working Group 2020
source: https://demo.codimd.org/GhjjMP9WRca71Pv8HgQ9iQ
---
Matérialités comparatives : Médias, littérature et théorie / Comparative Materialities: Media, Literature, Theory 

A project from Marcello Vitali-Rosati (marcello.vitali-rosati@sens-public.org), Université de Montréal, and Monique Tschofen (monique.tschofen@ryerson.ca), Ryerson University.

## Manifesto

En réponse à un billet de blog où Marcello essayait d'expliquer l'importance des formats et des outils d'écriture, un collègue twittait: "Encore un autre qui, au lieu que travailler, perd son temps à jouer avec LaTeX".

In response to a blog post where Marcello tried to explain the importance of formats and writing tools, a colleague tweeted: "Yet another one who, instead of working, wastes his time playing with LaTeX".

Il est intéressant d'analyser le point de vue qui est au fondement d'une telle réaction, car il représente une pensée fortement enracinée dans notre culture.
On pourrait dire que telle idée se situe dans la continuité d'une certaine interprétation de la fameuse critique platonicienne de l'écriture, développée dans le Phèdre. La position de Platon à ce sujet a été le centre de plusieurs débats - que l'on pense au texte de Derrida qui montre toute la complexité et les enjeux des ambiguïtés cachées dans le texte du philosophe grec. Si on lit Platon au premier degré on identifie une opposition entre l'idéalité de la pensée et l'impureté de son inscription matérielle: d'une part il y a ce qui compte vraiment, les contenus, les idées, dont l'expression la plus pure est le _logos_; de l'autre l'inscription matérielle de ces idées qui représente une forme de déchéance. La pureté supérieure de la pensée se transforme en un produit dérivé, bâtard, imparfait car incarné: l'écriture. C'est la traditionnelle opposition entre forme et matière où cette dernière est toujours une manifestation limitée et imparfaite de la première. Cette opposition se révèle par ailleurs de manière forte dans les genres masculins et féminin et on la retrouve aussi chez Aristote: une forme masculine et une matière féminine. C'est le sperme -- principe formel de vie -- qui se "nourrit" de la matière féminine pour s'incarner -- dans la théorie de la génération aristotélicienne, notamment.

It is interesting to analyze the point of view behind such a reaction, as it represents a way of thinking strongly rooted in our culture.
One could say that such an idea is in continuity with a certain interpretation of the famous Platonic criticism of writing, developed in the _Phaedrus_. Plato's position on this subject has been the focus of several debates - the most famous of which is Derrida's _La pharmacie de Platon_. If we read Plato in the first degree, we identify an opposition between the ideality of thought and the impurity of its material inscription: on the one hand there is what really counts, the content, the ideas, whose purest expression is the _logos_ (as if voice was not an inscription...); on the other hand the material inscription of these ideas which represents a form of decay. The superior purity of thought is transformed into a by-product, a bastard, imperfect product because it is embodied: writing. It is the traditional opposition between form and matter where the latter is always a limited and imperfect manifestation of the former. This opposition is also revealed in a strong way in the masculine and feminine genders and is also found in Aristotle: a masculine form and a feminine matter. It is the sperm - the formal principle of life - that "feeds" on female matter in order to be embodied - in the theory of the Aristotelian generation, in particular.



Une anecdote porphyrienne[^annotation] manifeste clairement cette idéologie: dans la Vie de Plotin, Porphyrie raconte que Plotin écrivait ses Énnéades pendant qu'il faisait autre chose: il parlait, il s'occupait d'autres affaires et en même temps il inscrivait sur un support la pensée complexe qu'il avait déjà développée. L'acte d'inscrire sa pensée sur un support est triviale, elle n'a en soi aucune importance et donc aucune dignité particulière. C'est un travail manuel, qui pourrait finalement être délégué à un individu sans aucune compétence, qui se limite à retranscrire, mécaniquement ce qui a été déjà élaboré. Pour citer d'autres conversations qui ont eu lieu autour du fameux billet de blog de Marcello, plusieurs collègues - toujours des hommes - soulignaient que le travail de mise en forme et de balisage des contenus devrait être laissé à "_une_ secrétaire". L'homme supérieur pense et crée le contenu. La femme, mécaniquement, inscrit ce contenu dans un support en réalisant ainsi un travail trivial, neutre et inintéressant.

A Porphyrian anecdote[^long] clearly manifests this ideology: in the Life of Plotinus, Porphyry tells how Plotinus wrote his Enneads while he was doing something else: he was talking, he was busy with other matters and at the same time he was writing down the complex thought that he had already developed. The act of inscribing his thought on a medium was considered trivial, it had no importance in itself and therefore no particular dignity. It is manual work, which could eventually be delegated to an individual without any competence, who just mechanically transcribe what has already been developed. To quote other conversations that took place around Marcello's blog post, several colleagues - always men - stressed that the work of formatting and tagging the contents should be left to "a secretary" (feminine in French). The superior man thinks and creates the content. The woman, mechanically, inscribes this content in a "delivery channel", thus carrying out a trivial, neutral and uninteresting work.


Il est significatif qu'en effet, dans l'histoire de l'informatique, les tâches "techniques" aient été traditionellement laissées aux femmes - que l'on regarde une photographie du laboratoire du Père Busa pour s'en rendre compte[^melissa].

![Les hommes regardent (Θέαομαι)...](https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Ftse1.mm.bing.net%2Fth%3Fid%3DOIP.6wf6pCXdjhUcYnmkrokuXAHaFl%26pid%3DApi&f=1)

![Les femmes inscrivent](https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fmelissaterras.files.wordpress.com%2F2013%2F10%2F9e1c4-0613.jpg&f=1&nofb=1)

It is significant that in the history of computer, "technical" tasks have traditionally been left to women - let's look at a photograph of Father Busa's laboratory to realize this.

L'idéologie dualiste qui voit une séparation nette entre forme et matière a une histoire longue donc et elle a été l'objet de plusieurs analyses et critiques - dont celle de Derrida est la plus connue. Cependant elle n'a jamais vraiment été dépassée. Elle est toujours là, et peut-être aussi dans les travaux de ceux qui ont le plus essayé de la critiquer. Derrida lui même finit par remplacer le concept de logos par une idée finalement assez immatérielle d'écriture et de texte.

The dualistic ideology that sees a clear separation between form and matter has a long history and it has been the object of several analyses and criticisms - Derrida's is the best known. However, it has never really been outdated. It is still there, and perhaps also in the works of those who have tried hardest to criticize it. Derrida himself ends up replacing the concept of logos with a rather immaterial idea of writing and text.


L'intérêt renouvellé pour la matérialité (que l'on pense à des mouvements tels que le new materialism) semble promettre des pistes différentes. Il faut penser la matérialité de l'écriture. Ou mieux: la pensée n'est que cette matérialité.

The renewed interest in materiality (all the "new materialism" wave) seems to promise different paths. One must think about the materiality of writing. Or better: thinking is only this materiality.


Penser littérature et média ensemble devrait permettre de penser une littérature toujours inscrite, ou encore mieux, penser que la littérature n'est qu'une inscription.

Thinking literature and media together should make it possible to think of literature as always inscribed, or even better, to think of literature as an inscription.


À partir de ces considération il semble évident que pour adresser ces question il est nécessaire de partir d'une inscription: celle à partir de laquelle une pensée critique peut émerger.

Starting from these considerations it seems obvious that in order to address these questions it is necessary to start from an inscription: the one from which a critical thought can emerge.


Que cela soit ce pad - et les différents protocoles de travail collectif qui peuvent y émerger - ou un projet git avec son dispositif de versionnage, ou plutôt un ensemble complexe de protocoles de communications (TCP/IP, HTTP etc.), formats (md, HTML, txt...), règles syntaxiques en différentes langues, outils et algorithmes - ce texte est en partie produit par deepl -... ce qui pense ici est l'incription.

Whether it is this pad - and the different collective work protocols that can emerge from it - or a git repository with its versioning device, or rather a complex set of communication protocols (TCP/IP, HTTP etc.), formats (md, HTML, txt...), syntax rules in different languages, tools and algorithms - this text is partly produced by deepl - ... what thinks here is the incription.

La letteratura è iscrizione materiale. Parlare di letteratura significa parlare di questa iscrizione materiale, fare letteratura è performare questa iscrizione, fare teoria della letteratura è interrogarsi - ancora performativamente - su questa iscrizione.

**Instead of "literature _and_ media" one could say: literature is media - if with media one means original material mediation, inscripton.** 

Cette inscriptoin est aussi le lieu possible d'émergence d'une individuation: ce que ce texte souhaite est que cette émergence individuante n'ait pas comme résultat un _individu_ mais plutôt un collectif.

This inscriptoin is also the possible place of emergence of an individuation: what this text wishes is that this individualizing emergence does not result in an _individual_ but rather in a collective.

E a proposito di iscrizioni...

>ὦ ξεῖν᾽, ἄγγειλον Λακεδαιμονίοις ὅτι τῇδε
κείμεθα, τοῖς κείνων ῥήμασι πειθόμενοι.
[Simonides - AP 7.249](http://anthologiagraeca.org/passages/urn:cts:greekLit:tlg7000.tlg001.perseus-grc2:7.249)

[Bibliography is here](https://www.zotero.org/groups/2519435/literatureandmedia/items) - please add references!

## Some other little things

If thinking is inscription, do not leave thinking to private companies: formats, protocols and tools must be open and free (as freedom, not as free beer). Please let us avoid to reduce a collective work to a Microsoft production.

## Members
Joshua Synenko
Marcello Vitali-Rosati
Margot Mellet
Jeanne Mathieu-Lessard
Markus Reisenleitner
Monique Tschofen
Antoine Fauchié
Kelly Egan
Eugénie Matthey-Jonais
Alevtina Lapiy
Irina Sadovina

## Introductions
**Monique Tschofen**: I am an Associate Professor in English at Ryerson University, and affiliated with the Joint York-Ryerson grad program in Communications and Culture. My background is a PhD in Comparative Literature and Film Studies at the U of Alberta some million years ago. My current research is on avant-garde and vernacular cinema, digital installation, poetry, the novel, and photography. I see my role here as a facilitator as much as a scholar, and want to create a space for whatever kinds of collaborative inquiry we feel we need as a group.

**Markus Reisenleitner**: I am a Professor of Humanities at York University, where I am also affiliated with the Graduate Program in Communication and Culture, and editor-in-chief of [Imaginations](http://imaginations.glendon.yorku.ca/), a multilingual, open-access journal of international visual cultural studies. My background is in Cultural Studies. I am interested in Urban Culture, Digital Culture, academic publishing as a cultural practice, and the lineages of the Counterculture. I am currently working on a monograph on the countercultural lineages of Silicon Valley and its offsprings, Silicon Beach and Silicon Slopes, which also takes me back to a personal history of an abandoned career as a software developer and consultant during the 1990s, when the internet still seemed to be a promise of a commons for communal knowledge creation.

**Marcello Vitali-Rosati**: I am an Associate Professor in the Department of French Literature at the University of Montréal and chairholder for the Canada Research Chair on Digital Textualities. My research offers a philosophical reflection on digital technologies and the issues pertaining to them, including concepts relating to the virtual, to digital identity, to the author and authorship, to forms of production as well as to the dissemination and legitimization of knowledge in the digital age. In addition, I am one of the most active contributors of the theory of editorialization.I am the author of several articles and monographs. I am also editor in chief of the journal Sens Public and co-director of the “Parcours Numériques" collection at the Presses de l’Université de Montréal (PUM).As chairholder of the Canada Research Chair on Digital Textualities, I also direct several digital humanities projects, particularly as pertains to the scholarly publishing field. Within this framework, I direct the development of journal editing and augmented monograph platforms, editing software and an editing platform for the collaborative edition of the Greek Anthology.

**Margot Mellet**: I am a Ph.D candidate in French Literature at the Université de Montréal in Research and Creation. My project focuses on the palimpsest, as a process of remediation of a medium, to understand how the medium becomes an instance of literary enunciation. I am also a student member of CRIalt (Centre de Recherches Intermédiales sur les arts, les lettres et les techniques) and editorial coordinator of the magazine Sens public. Scientific coordinator of the Canada Research Chair in Digital Writing, I coordinate the collaborative digital publishing project of the Anthologie Palatine.

**Antoine Fauchié**: I am a Ph.D candidate at University of Montréal. My research focuses on the process of publication.

**Eugénie Matthey-Jonais**: I am a Master's student in the Department of French Literature at the University of Montréal, writing my thesis under the supervision of Catherine Mavrikakis and Marcello Vitali-Rosati. My research focuses on various political and philosophical aspects of the work of Marguerite Duras. I am editorial assistant for the journal Sens Public, where I pursue my interest in digital publishing.

**Joshua Synenko**: I am Assistant Professor in the Department of Cultural Studies at Trent University. I serve as Coordinator of the Media Studies undergraduate program, and I will be doing the same for the Cultural Studies MA and PhD programs in January-June 2021. My work explores subjects in critical data studies, memory culture, urbanism & visual studies. This year, I’m working on two projects. The first is a work of media sociology in that it looks at policing and safety apps like Citizen, and critically examines how these apps help to shape urban lives and spatial imaginaries of the city, as well as policy debates. My second project examines some issues surrounding digital images, questioning the materiality of images in relation to the history of photorealism and digital mediation in works like Farocki’s (2012) *Parallel I-IV*, and more recent works like Parikka and Gil-Fournier’s (2019) *Seed, Image, Ground*.

**Lai-Tze Fan**: I am an Assistant Professor in English at the University of Waterloo, where I work out of the Critical Media Lab and the Games Institute. I am also an Editor and the Director of Communications of electronic book review, a Co-Editor of the digital review, and the Editor of Media & Digital Culture of Reviews in Cultural Theory. I did my PhD in Communication and Culture, under Monique T. and Markus R. ^_^ Some research buzzwords: digital storytelling, media and technology theory, gender and technological design, creative DH (especially electronic literature and critical game design), maker culture, research-creation.

**Alevtina Lapiy**: I am a Master’s Student in the Literatures of Modernity program at Ryerson. In the spring semester, I will be assisting Dr. Tschofen in her research on vernacular theory and short film. I see my role in this group as a listener. I am eager to learn about the intersections between film, media, and theory, so work that may seem trite to you, may be exciting to me! Please reach out should you want research assistance or a copy edit.

**Christina Anto**: Hello! I’m a Master’s Student in Literatures of Modernity at Ryerson and my area of focus is video game theory and modes of analysis. I’d love to connect over developments in theoretical practices and learn more about the more technical aspects of games. 

**Jeanne Mathieu-Lessard**: J'ai fait mon doctorat en littérature comparée à l'Université de Toronto et je suis chargée de cours au Département des littératures de langue française à l'Université de Montréal. Je m'intéresse au mouvement entre langues dans le texte littéraire et dans la critique and I'm hoping academia can find creative ways to integrate multilingualism and be more inclusive and fluid in its approach and formats.

**Guido O. Gagnon**: As a scholar working at the intersection of digital humanities and comparative literature, I am particularly interested in questions of knowledge formation and authorship and in new ways to think about the dissemination of our work and of our identities. I can be contacted at guido-o-gagnon@sens-public.org.

###### tags: `CCLA`

[^annotation]: [annotation here](https://hyp.is/3BA4pq5nEeq7ZfMntgc1ng/sacred-texts.com/cla/plotenn/enn001.htm)
[^long]: I myself, Porphyry of Tyre, was one of Plotinus' very closest friends, and it was to me he entrusted the task of revising his writings. 8. Such revision was necessary: Plotinus could not bear to go back on his work even for one re-reading; and indeed the condition of his sight would scarcely allow it: his handwriting was slovenly; he misjoined his words; he cared nothing about spelling; his one concern was for the idea: in these habits, to our general surprise, he remained unchanged to the very end.   
He used to work out his design mentally from first to last: when he came to set down his ideas, he wrote out at one jet all he had stored in mind as though he were copying from a book. Interrupted, perhaps, by someone entering on business, he never lost hold of his plan; he was able to meet all the demands of the conversation and still keep his own train of thought clearly before him; when he was fee again, he never looked over what he had previously written--his sight, it has been mentioned, did not allow of such re-reading--but he linked on what was to follow as if no distraction had occurred. Thus he was able to live at once within himself and for others; he never relaxed from his interior attention unless in sleep; and even his sleep was kept light be an abstemiousness that often prevented him taking as much as a piece of bread, and by this unbroken concentration upon his own highest nature.[text annotated here](https://hyp.is/3BA4pq5nEeq7ZfMntgc1ng/sacred-texts.com/cla/plotenn/enn001.htm)
[^melissa]: [Melissa Terra en parle dans son travail sur Lovecraft](https://melissaterras.org/2013/10/15/for-ada-lovelace-day-father-busas-female-punch-card-operatives/)