---
title: Bibliography
bibliography: data/bibliography.bib
csl: data/mla.csl
nocite: |
  @*
---
{{< bibliography "data/bibliography.json" >}}
