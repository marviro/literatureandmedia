---
title: "CCLA Annual Meeting : Les nouvelles voies de la littérature comparée / Changing Directions in Comparative Literature"
subtitle: "Panel III"
dateevent: 2021-05-13
date: 2021-05-10
layout: conference
pad: "https://annuel2.framapad.org/p/6g9zqc2ipm-9mzu"
explications: "Please read the instructions on the left / Merci de lire les instructions indiquées sur la gauche"
concept_en: "The objective of the panel is to respond in a performative and simultaneous way to several questions stated and documented on a collaborative pad (on the right)."
concept_fr: "L'objectif du panel est de répondre de manière performative et simultanée à plusieurs questions posées et documentées dans un pad collaboratif (sur la droite)."
meet: "https://meet.jit.si/cclaaclc2021inscriptions"
informations: "14h30-17h - 13 mai<br>Workshop: <em>Enacting Inscription</em><br>Moderator: Marcello Vitali-Rosati"
group_title: "Research Group Presentation"
presentation: "The goal of the CCLA Research Groups is to build an intellectual community of comparatists in Canada and around the world. We wish to meet and think together about the core premises and methods of the discipline, and foster the kinds of innovative scholarship practices that will take us into the next decade and beyond."
question: "The Main question of Group Comparative Materialities: Media, Literature, Theory / groupe de recherche Matérialités comparatives: médias, littérature et théorie:"
question_en: "What are ways of “doing” comparative literature that can attend to the materiality of texts as well as of communication itself, considering medium, affordances, platforms, and interfaces?"
question_fr: "Comment 'faire' de la littérature comparée qui en s'intéressant à la matérialité des textes ainsi qu'à la communication elle-même, en considérant le support, les moyens, les plateformes et les interfaces ?"

---


## The Tool for writing performance : *[Framapad](https://semestriel.framapad.org/)*

- What is Framapad ? / Framapad, c'est quoi ?

Framapad is an onligne collaborative editor for pad. / un éditeur de pad collaboratif en ligne.

- How to use Framapad ? / Framapad, comment on l'utilise ? 

In the pad we shared with you (on the right), you may start to write your text. / Sur le pad partagé pour l'occasion (à votre droite), vous pouvez commencer à écrire. 

Each participant will be distinguished by a color. You will discover your attribute color as soon as you write on the pad. / À chaque participant, une couleur est attribuée. Pour la découvrir, il vous suffit de commencer à écrire. 

You have also the possibility to chat with other participant in the groupe. / vous avez également la possibilité de discuter avec d'autres participants du groupe. 

Demonstration [here / ici](https://hebdo.framapad.org/p/bac-a-sable-9mzs)
