---
title: "Notes (brainstorming)"
source: https://demo.codimd.org/vBfQqNJgT4a63dYxbCxSDg
---
## Avertissement / short-notice!

**Please play with this text as you like: delete, copy and paste, add references, discuss it, put it elsewhere...**

This text will be versioned with git.

- if you are familiar with markdown and would like to write, add, cut, destroy it all, [you can play with the text here](https://demo.codimd.org/vBfQqNJgT4a63dYxbCxSDg?both#)
- if you are familiar with git, [you can play with the text here](https://framagit.org/marviro/literatureandmedia)
- if you are familiar with zotero, [you can play with biblography here](https://www.zotero.org/groups/2519435/literatureandmedia/items)
- if you like: [here is a published version of the text](http://notes.ecrituresnumeriques.ca/vBfQqNJgT4a63dYxbCxSDg.html) that can be annotated with hypothes.is

**If you are not familiar with this stuff but you would like to... please ask Marcello for a little tutorial**

## Ouverture / Opening

En réponse à un billet de blog où Marcello essayait d'expliquer l'importance des formats et des outils d'écriture, un collègue twittait: "Encore un autre qui, au lieu que travailler, perd son temps à jouer avec LaTeX".

In response to a blog post where Marcello tried to explain the importance of formats and writing tools, a colleague tweeted: "Yet another one who, instead of working, wastes his time playing with LaTeX".

Il est intéressant d'analyser le point de vue qui est au fondement d'une telle réaction, car il représente une pensée fortement enracinée dans notre culture.
On pourrait dire que telle idée se situe dans la continuité d'une certaine interprétation de la fameuse critique platonicienne de l'écriture, développée dans le Phèdre. La position de Platon à ce sujet a été le centre de plusieurs débats - que l'on pense au texte de Derrida qui montre toute la complexité et les enjeux des ambiguïtés cachées dans le texte du philosophe grec. Si on lit Platon au premier degré on identifie une opposition entre l'idéalité de la pensée et l'impureté de son inscription matérielle: d'une part il y a ce qui compte vraiment, les contenus, les idées, dont l'expression la plus pure est le _logos_; de l'autre l'inscription matérielle de ces idées qui représente une forme de déchéance. La pureté supérieure de la pensée se transforme en un produit dérivé, bâtard, imparfait car incarné: l'écriture. C'est la traditionnelle opposition entre forme et matière où cette dernière est toujours une manifestation limitée et imparfaite de la première. Cette opposition se révèle par ailleurs de manière forte dans les genres masculins et féminin et on la retrouve aussi chez Aristote: une forme masculine et une matière féminine. C'est le sperme -- principe formel de vie -- qui se "nourrit" de la matière féminine pour s'incarner -- dans la théorie de la génération aristotélicienne, notamment.

It is interesting to analyze the point of view behind such a reaction, as it represents a way of thinking strongly rooted in our culture.
One could say that such an idea is in continuity with a certain interpretation of the famous Platonic criticism of writing, developed in the _Phaedrus_. Plato's position on this subject has been the focus of several debates - the most famous of which is Derrida's _La pharmacie de Platon_. If we read Plato in the first degree, we identify an opposition between the ideality of thought and the impurity of its material inscription: on the one hand there is what really counts, the content, the ideas, whose purest expression is the _logos_ (as if voice was not an inscription...); on the other hand the material inscription of these ideas which represents a form of decay. The superior purity of thought is transformed into a by-product, a bastard, imperfect product because it is embodied: writing. It is the traditional opposition between form and matter where the latter is always a limited and imperfect manifestation of the former. This opposition is also revealed in a strong way in the masculine and feminine genders and is also found in Aristotle: a masculine form and a feminine matter. It is the sperm - the formal principle of life - that "feeds" on female matter in order to be embodied - in the theory of the Aristotelian generation, in particular.



Une anecdote porphyrienne^[[annotation here](https://hyp.is/3BA4pq5nEeq7ZfMntgc1ng/sacred-texts.com/cla/plotenn/enn001.htm)] manifeste clairement cette idéologie: dans la Vie de Plotin, Porphyrie raconte que Plotin écrivait ses Énnéades pendant qu'il faisait autre chose: il parlait, il s'occupait d'autres affaires et en même temps il inscrivait sur un support la pensée complexe qu'il avait déjà développée. L'acte d'inscrire sa pensée sur un support est triviale, elle n'a en soi aucune importance et donc aucune dignité particulière. C'est un travail manuel, qui pourrait finalement être délégué à un individu sans aucune compétence, qui se limite à retranscrire, mécaniquement ce qui a été déjà élaboré. Pour citer d'autres conversations qui ont eu lieu autour du fameux billet de blog de Marcello, plusieurs collègues - toujours des hommes - soulignaient que le travail de mise en forme et de balisage des contenus devrait être laissé à "_une_ secrétaire". L'homme supérieur pense et crée le contenu. La femme, mécaniquement, inscrit ce contenu dans un support en réalisant ainsi un travail trivial, neutre et inintéressant.

A Porphyrian anecdote^[I myself, Porphyry of Tyre, was one of Plotinus' very closest friends, and it was to me he entrusted the task of revising his writings.   
8. Such revision was necessary: Plotinus could not bear to go back on his work even for one re-reading; and indeed the condition of his sight would scarcely allow it: his handwriting was slovenly; he misjoined his words; he cared nothing about spelling; his one concern was for the idea: in these habits, to our general surprise, he remained unchanged to the very end.   
He used to work out his design mentally from first to last: when he came to set down his ideas, he wrote out at one jet all he had stored in mind as though he were copying from a book.   
Interrupted, perhaps, by someone entering on business, he never lost hold of his plan; he was able to meet all the demands of the conversation and still keep his own train of thought clearly before him; when he was fee again, he never looked over what he had previously written--his sight, it has been mentioned, did not allow of such re-reading--but he linked on what was to follow as if no distraction had occurred.   
Thus he was able to live at once within himself and for others; he never relaxed from his interior attention unless in sleep; and even his sleep was kept light be an abstemiousness that often prevented him taking as much as a piece of bread, and by this unbroken concentration upon his own highest nature.[text annotated here](https://hyp.is/3BA4pq5nEeq7ZfMntgc1ng/sacred-texts.com/cla/plotenn/enn001.htm)] clearly manifests this ideology: in the Life of Plotinus, Porphyry tells how Plotinus wrote his Enneads while he was doing something else: he was talking, he was busy with other matters and at the same time he was writing down the complex thought that he had already developed. The act of inscribing his thought on a medium was considered trivial, it had no importance in itself and therefore no particular dignity. It is manual work, which could eventually be delegated to an individual without any competence, who just mechanically transcribe what has already been developed. To quote other conversations that took place around Marcello's blog post, several colleagues - always men - stressed that the work of formatting and tagging the contents should be left to "a secretary" (feminine in French). The superior man thinks and creates the content. The woman, mechanically, inscribes this content in a "delivery channel", thus carrying out a trivial, neutral and uninteresting work.



Il est significatif qu'en effet, dans l'histoire de l'informatique, les tâches "techniques" aient été traditionellement laissées aux femmes - que l'on regarde une photographie du laboratoire du Père Busa pour s'en rendre compte^[[Melissa Terra en parle dans son travail sur Lovecraft](https://melissaterras.org/2013/10/15/for-ada-lovelace-day-father-busas-female-punch-card-operatives/)].

![Les hommes regardent (Θέαομαι)...](https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Ftse1.mm.bing.net%2Fth%3Fid%3DOIP.6wf6pCXdjhUcYnmkrokuXAHaFl%26pid%3DApi&f=1)

![Les femmes inscrivent](https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fmelissaterras.files.wordpress.com%2F2013%2F10%2F9e1c4-0613.jpg&f=1&nofb=1)

It is significant that in the history of computer, "technical" tasks have traditionally been left to women - let's look at a photograph of Father Busa's laboratory to realize this.

L'idéologie dualiste qui voit une séparation nette entre forme et matière a une histoire longue donc et elle a été l'objet de plusieurs analyses et critiques - dont celle de Derrida est la plus connue. Cependant elle n'a jamais vraiment été dépassée. Elle est toujours là, et peut-être aussi dans les travaux de ceux qui ont le plus essayé de la critiquer. Derrida lui même finit par remplacer le concept de logos par une idée finalement assez immatérielle d'écriture et de texte.

The dualistic ideology that sees a clear separation between form and matter has a long history and it has been the object of several analyses and criticisms - Derrida's is the best known. However, it has never really been outdated. It is still there, and perhaps also in the works of those who have tried hardest to criticize it. Derrida himself ends up replacing the concept of logos with a rather immaterial idea of writing and text.



L'intérêt renouvellé pour la matérialité (que l'on pense à des mouvements tels que le new materialism) semble promettre des pistes différentes. Il faut penser la matérialité de l'écriture. Ou mieux: la pensée n'est que cette matérialité.

The renewed interest in materiality (all the "new materialism" wave) seems to promise different paths. One must think about the materiality of writing. Or better: thinking is only this materiality.



Penser littérature et média ensemble devrait permettre de penser une littérature toujours inscrite, ou encore mieux, penser que la littérature n'est qu'une inscription.

Thinking literature and media together should make it possible to think of literature as always inscribed, or even better, to think of literature as an inscription.



À partir de ces considération il semble évident que pour adresser ces question il est nécessaire de partir d'une inscription: celle à partir de laquelle une pensée critique peut émerger.

Starting from these considerations it seems obvious that in order to address these questions it is necessary to start from an inscription: the one from which a critical thought can emerge.



Que cela soit ce pad - et les différents protocoles de travail collectif qui peuvent y émerger - ou un projet git avec son dispositif de versionnage, ou plutôt un ensemble complexe de protocoles de communications (TCP/IP, HTTP etc.), formats (md, HTML, txt...), règles syntaxiques en différentes langues, outils et algorithmes - ce texte est en partie produit par deepl -... ce qui pense ici est l'incription.

Whether it is this pad - and the different collective work protocols that can emerge from it - or a git repository with its versioning device, or rather a complex set of communication protocols (TCP/IP, HTTP etc.), formats (md, HTML, txt...), syntax rules in different languages, tools and algorithms - this text is partly produced by deepl - ... what thinks here is the incription.

La letteratura è iscrizione materiale. Parlare di letteratura significa parlare di questa iscrizione materiale, fare letteratura è performare questa iscrizione, fare teoria della letteratura è interrogarsi - ancora performativamente - su questa iscrizione.

**Instead of "literature _and_ media" one could say: literature is media - if with media one means original material mediation, inscripton.** 

Cette inscriptoin est aussi le lieu possible d'émergence d'une individuation: ce que ce texte souhaite est que cette émergence individuante n'ait pas comme résultat un _individu_ mais plutôt un collectif.

This inscriptoin is also the possible place of emergence of an individuation: what this text wishes is that this individualizing emergence does not result in an _individual_ but rather in a collective.

E a proposito di iscrizioni...

>ὦ ξεῖν᾽, ἄγγειλον Λακεδαιμονίοις ὅτι τῇδε
κείμεθα, τοῖς κείνων ῥήμασι πειθόμενοι.
[Simonides - AP 7.249](http://anthologiagraeca.org/passages/urn:cts:greekLit:tlg7000.tlg001.perseus-grc2:7.249)

[Bibliography is here](https://www.zotero.org/groups/2519435/literatureandmedia/items) - please add references!

## Some other little things

If thinking is inscription, do not leave thinking to private companies: formats, protocols and tools must be open and free (as freedom, not as free beer). Please let us avoid to reduce a collective work to a Microsoft production.

## Practical aspects

- imagination issue?
- meetings for mentoring on practical aspects (digital writing etc.) and theoretical (cinema and literature?)
- a panel for the conference?
- 3 meetings in fall?
- Bring your own work meetings to workshop works in progress
- "theory"! -- "theoretical and practical aspects"
- materiality in virtual environments!
- community building


## Questions
- how is it possible to transform a living document in something that can be published? How can a document like this become knowledge?
- how theory emerges? -> Crystals of knowledge
- what is compLit?
- how much do we share ?

## Timeline / Tasks
- Organize meeting with other Research Groups - Jeanne emails right now
- Everyone: write a few sentences describing how you envision the group and its work. 
- Make the 1-pager description: model on the others' 
  
- What seminar would you run, when?
- When do we invite specific people?
- When do we invite a larger group?

## Notes toward one-pager

Build a space  where people can talk: writing protocol, dissemination of content, delivery channel.   
Can we imagine, build, and manage spaces which allow us to meet, think together?


## To discuss the first meeting

### 1. Group's Title: (brainstorming)

- Literature and media? 
- Comparative Media, Materiality, Representation?
- Media across borders?
- Comparative Theory??
- Comparative Materialities: Media, Literature, Theory?
- Building Thinking


### 2. Group's Mandate:

- building research communities
  Create space for collaborative research/ collaborate & suppport individual research
    - mentor young scholars
    - expand reach of discipline & organization
        --> writing spaces
        --> seminars: 
            - Marcello and Markus and Jeanne on open publishing.
            (is there other expertise here?)
        --> working groups / more informal reading groups?? 
        
        
### 3. Creation of infrastructure (what, how, to what ends, how to circulate & store?):

    - Marcelo's crowdsourced document work  towards a manifesto: state of the (new) discipline?)
    - metareflection on format of scholarship: 
    - delivery channel 
        - oral / zoom 
        - paper
        - digital
- legitimation: 
    - how do young scholars get line on their cv? DH principles. 
    - “crystals of knowledge” 
    - organize dossier with journals for outputs: Sens public, imaginations, more creative & experimental. 

     - ,    
-

### 4. 

### Members > [can we get email addresses here?]
- Monique Tschofen
- Marcello Vitali-Rosati
- Margot Mellet
- Markus Reisenleitner
- Jeanne Mathieu-Lessard
- Joshua Synenko
- Antoine Fauchié?
- Servanne Monjour?
- Nicolas Sauret?
- Lee Campbell
- Nanditha Narayanamoorthy
- Irina Sadovina : irina.sadovina@gmail.com

People to get involved:
- Laurence Sylvain
- Angela Joosse
- Daniel Browne?
- Lai-Tze Fan
- Brent Bellamy

## Comparative Materialities: Media, Literature, Theory / Matérialités comparatives: Médias, littérature et théorie
Penser le littéraire implique de se questionner sur son inscription matérielle et sur les relations entre médias qui informent nos lectures du monde. Ce groupe de recherche souhaite se pencher sur la relation indissociable entre le développement de toute pensée et de tout discours et leur support, et sur ce qu'une approche comparatiste permet de mettre en évidence dans notre rapport aux médias.

Thinking about literature leads us to question its material inscription and the relationship between media that informs our interpretation of the world. This research group will look at the interconnection between our ways of thinking and our discourses, and their material support, and will discuss how a comparative approach can shed light on our relationship with media.

(I love this jeanne: I think I might have made it worse, but wanted to present the range of possibilities:

This research group will explore how a comparative approach to media can shed light on the interconnection between our ways of thinking and discourses, and their material supports. Thinking literature /culture and media together should make it possible to think of culture as always inscribed, or even better, as an inscription. A multitude of approaches to literature, cinema, digital and networked culture, visual culture, transmedia and so on are welcome including media archaeology, critical making, __?others??__ 
 )

@Monique, Jeanne, Marcello: here is my stab at following this line of thought:

This research group will look at historical and contemporary interconnections, relationships and entanglements between manifestations of thinking/writing across cultures/languages and their foundations in the materialities of media technologies. These materialities are determined by sociocultural contexts and relations to power, privilege, and ownership. Thinking literature /culture and media together should make it possible to think of culture as always inscribed, or even better, as an inscription in complex assemblages that inform our interpretation of the world. In order to explore these assemblages, the research group welcomes a multitude of comparative and theoretical approaches to literature, cinema, digital and networked culture, visual culture, and transmedia, including media archaeology, critical making, and comparative media studies. Literature study in particular brings a rich history and context to media study; it allows us to situate narrative and expressive forms in the otherwise cold methodology of communication theories, and therefore to engage with mediations as networks of relationships that are historically embedded in literary conventions and techniques, not divorced from them. 

Ce groupe de recherche s'intéresse aux interconnexions, relations et enchevêtrements historiques et contemporains entre les manifestations de notre pensée et de notre écriture à travers les cultures et les langues, et leurs fondements dans les matérialités des technologies médiatiques. Ces matérialités  sont déterminées par des contextes socioculturels et des relations au pouvoir, au privilège et à la propriété. Penser la littérature, la culture et les médias ensemble permet de penser la culture comme toujours "inscrite", ou mieux encore, comme une inscription au sein d'assemblages complexes qui guident notre interprétation du monde. Afin d'explorer ses assemblages, le groupe de recherche souhaite accueillir une multitude approches comparative et théoriques de la littérature, du cinéma, de la culture numérique et en réseau et de la culture visuelle et transmédiatique, notamment l'archéologie médiatique, la recherche-action et les études médiatiques comparées. Les études littéraires permettent de penser les études médiatiques au sein d'une histoire et d'un contexte riches; elles nous permettent de situer des formes narratives et expressives dans la méthodologie autrement froide des théories de la communication, et ainsi de s'inscrire au sein de médiations comme réseaux de relations ancrées dans des conventions littéraires et techniques, plutôt que divorcées d'elles.

## Sessions organisation: 3 Ateliers/WorkSessions/Workshops

- presentation/discussion 
- proposer des ateliers de formation bilingues 
- Deux ou trois séances (1h30 each) : + presentation of the group / introduction people
    1. Introduction & moderate discussion : enjeux de l'écriture (écriture et matérialité de l'environnement numérique) : lister les sujets que l'on souhaite aborder (5-10 minutes for each presentor)
        - briefly present the 3 sessions + present differences between  Full text / format like plain text : show what a docx doc is really 
        - Material text/inscription & formats aren't neutral or free, how you writes matters in the product: **Materiality Matters**, examples of using API @antoine (ateliers, stylo export) @marcello - important to talk about text formats: full text as the lower knowledge model and then formats like docx
        - nature of text & mediation : how literature is describe/fluidity of text in digital age/question of representation, API, transfert **Revisit Representation** (representation of knowledge, what constitute knowledge and what constitute sources of knowledge) : list papers & sources --> Schnapp *Knowledge Design* & McGuire (blogpost) *a Publisher's job is to provide a good API* (http://toc.oreilly.com/2013/02/a-publishers-job-is-to-provide-a-good-api-for-books.html) @markus
        - toolchain/publishing chain like Les Ateliers @antoine
        - talk about formats (semantics and graphics) : stylo export format @margot = **Formats make differents objects** 
        - use the Markdown/Stylo tutorial: https://gitlab.huma-num.fr/ecrinum/manuels/tutoriel-markdown-pandoc + https://artorig.github.io/tutomd/
        - pratical exemple with zotero 
 
     2. Markdown & Semantic writing @antoine & eugénie & margot
        - introduction @margot
        - Stylo Documentation both in French and English. @Margot @Antoine 
        - pratical exercice as in [markdownTutorial](https://artorig.github.io/tutomd/) @antoine
        - user experience @eugenie maybe with student journal?

    3. Git & Versioning
        - introduction by @Markus about git. Practices and how they intersect
        - pratical : @antoine
        - workshop offering

###### tags: `CCLA`
