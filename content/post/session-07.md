---
date: 2021-04-27
dateevent: 2021-05-04
title: "7th session: Public Lecture: Embodied Materialities / Matérialités incarnées"
source: https://demo.hedgedoc.org/eQ6fjB7WTzKEZxeRbqvSrw
summary: "Jennifer O’Connor, Angela Joosse and Monique Tschofen"
home: yes
---
Speakers:

- “Menstrala: Gender, Art, and Blood” by Jennifer O’Connor
- “‘Where am I now?’ Embodied Relational Process inP. Megan Andrews’ Disorientation Project” by Angela Joosse
- “Becoming M/other: Materialities in Ali Abbasi’s 2018 film Border” by Monique Tschofen

Mardi 4 mai, 2021, à midi (HNE)  
Tuesday, May 4, 2021, 12:00 EST

Online / en ligne:  
[https://meet.jit.si/CCLA_materialities_2021](https://meet.jit.si/CCLA_materialities_2021)

## Menstrala: Gender, Art, and Blood
**Jennifer O’Connor**

How has menstruation been understood, categorized, and pathologized? In what ways has this affected the construction of “woman”? Can feminists interpret these ideas through art in a way that challenges or provokes? That creates a feminist conception of the body? These are the questions that interest me. By examining the work of five contemporary Canadian artists in relation to Ranciere’s writing on dissensus—with interventions from Lacan (objet a), Kristeva (the abject), and others—I will critique menstrual art across media and illustrate how this work may inform social and political thought.

## “Where am I now?” Embodied Relational Process in P. Megan Andrews’ Disorientation Project
**Angela Joosse**

Through the Disorientation Project, dance artist P. Megan Andrews commits to entering experiences of embodied disorientation, lingering there to the point where one can begin to notice the impulses towards reorientation, order, and normalcy that commonly remain concealed. Working at the levels of perception and gesture, Andrews is tackling invisible structures that continue to hold things like colonialism, racism, patriarchy, and heteronormativity in place. By turning to her body and consciously entering experiences of disorientation, she is cultivating more restorative relations with self, objects, others, and environment. This paper reflects on the integral role of relational process in the Disorientation Project, particularly from my own perspective as a collaborative witness.

## Becoming M/other: Materialities in Ali Abbasi’s 2018 film Border
**Monique Tschofen**

Iranian-Swedish filmmaker Ali Abbasi’s 2018 fairy tale/fantasy-indebted film Border offers a politico-philosophical study of the body as medium and intermedium. Telling the story of a border agent Tina, whose extraordinary sense of smell has been put to use in ferry terminals sniffing out fear, anxiety, and lies told by people whose deceits range from smuggling alcohol to child snuff pornography, the film presents her wilding—a radical, jubilant, erotic repossession of the body. I bring object-oriented and phenomenological feminism to illuminate the film’s treatment of the sense of touch, that sense which Arisotle says is shared among all living beings, to tease out the broader political implications of the film’s study of twinned gestures of approach and retreat, penetration and withdrawal, immersion and expulsion.

Taking notes/prise de note: [https://demo.hedgedoc.org/6Z4lGuWCT9Og_eF_xYZf2A](https://demo.hedgedoc.org/6Z4lGuWCT9Og_eF_xYZf2A)