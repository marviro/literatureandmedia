---
date: 2023-09-27 
dateevent: 2023-10-30 
title: "A Research-creation Episteme? Pratice-based Reseach and Institutional Critique"
source: https://docs.google.com/document/d/1DqBRcmKXK7SQk4wT5FtrGfLq8K4lwzMc4KdHBXHHe0c/edit?usp=sharing
summary: "Hybrid Symposium at Trent University | Peterborough ON, Canada in Cooperation with: Materialities Research Group, Canadian Comparative Literature Association (complit.ca), Cultural Studies Graduate Programs, Trent University (trentu.ca), Public Texts Graduate Program, Trent University (trentu.ca) and the Imaginations: Journal of Cross-Cultural Image Studies (imaginationsjournal.ca) with guest: Prof. Laura Marks (Simon Fraser University)"
home: yes
---

The event is free and open to the public ! 

To see [the registration instructions and the symposium program](https://docs.google.com/document/d/1DqBRcmKXK7SQk4wT5FtrGfLq8K4lwzMc4KdHBXHHe0c/edit?usp=sharing)

The symposium is in cooperation with: 

- Materialities Research Group, [Canadian Comparative Literature Association](https://complit.ca/), 
- Cultural Studies Graduate Programs, [Trent University](https://www.trentu.ca/)
- Public Texts Graduate Program, [Trent University](https://www.trentu.ca/)
- [Imaginations: Journal of Cross-Cultural Image Studies](https://imaginations.space/)

with Guest: Prof. Laura Marks (Simon Fraser University)

and organized by:

- Agata Mergler, Université de York (agatamer@yorku.ca)
- Joshua Synenko, Université de Trent (joshuasynenko@trentu.ca)

--- 

Call of Paper of the Symposium : 
(*french follows*)

> Dissensus is not a confrontation between interests or opinions. It is the demonstration (manifestation) of a gap in the sensible itself. (Jacques Rancière)

Humanities scholars have been asking increasingly specific questions about whether creative practices correlate to knowledge production, and about the boundaries of a creative research orientation. While systems of categorization have shifted to meet new demands for knowledge transfer and dissemination at universities, the watersheds protecting visual artistic practice have given way to multi-modal forms of expression.

These include archival projects, creative writing, communications, documentary film, film essay, mapping and locative projects, sound art, theatre and performance, transmedial storytelling, and others.

The epistemic shift implied by reconfiguring these outputs as novel practices that either complement or displace the traditional pathways of knowledge production is still largely untested despite strong initial recognition by research granting agencies, faculty hiring committees, and other pockets of institutional power.

Our daylong (hybrid) symposium at Trent University (Peterborough, Ontario, Canada) will examine these issues by asking the following:

- What is the boundary dividing creative from non-creative practices?
- What are approaches that artists have adopted to transfer their practice into a knowledge-producing milieu?
- What are the fora of presenting research-creative projects, and who are the audiences?
- How does academic research-creation respond to the demand for community involvement and accountability? What are the institutional guarantees?
- How has artistic value been redirected to meet the university’s market demands (distinct from those of the culture industries)?
- How do creative works develop pathways through the established benchmarks of securing research funding? How have funding agencies responded to these shifts, and are they viable?
- What are the implications of developing work under different linguistic, national, regional, or global conceptual umbrellas (e.g. “practice-based” vs. “practice-led”)?
- What does research-creation entail for undergraduate teaching, graduate supervision and mentorship?
- How does a research-creative knowledge form comply with the evaluation rubrics for hire, reappointment, tenure, and promotion?
- How do creative outputs advance causes of equity and access?
- To what extent does research-creation, modelled as an intervention, participate in the ongoing labour to decolonize universities?
- What does research-creation reveal for reputedly “traditional” researchers about their own practices?
- What are such researchers afraid of when they encounter research-creative projects?
- What does the diversification of knowledges and methods add to historical debates on the subject and whom do these serve?

While many of the necessary questions have been asked before, consensus has not been achieved and perhaps never should. The developing social contract on research-creation may result in outcomes less desirable than the existing state of affairs. We therefore aim to build a community of scholars linked by solidarity as opposed to unanimity and welcome dissension into our ranks. Instead of demanding answers we seek better questions.

We invite scholars, including faculty and graduate students, artists, practitioners, creatives, and collaborators to deliver short (~ 5 minute) “manifestos” that explore at least one aspect of the emerging episteme. These will be convened in a daylong event at Trent University (Peterborough, Ontario, Canada), as well as remotely via Zoom.

----

> Le dissensus n’est pas la confrontation des intérêts ou des opinions. Il est la démonstration (manifestation) d’un écart du sensible à lui-même. (Jacques Rancière)

Les chercheurs en sciences humaines se posent des questions de plus en plus précises sur la corrélation entre les pratiques créatives et la production de connaissances, ainsi que sur les limites d’une orientation de recherche créative. Alors que les systèmes de catégorisation ont évolué pour répondre aux nouvelles exigences de transfert et de diffusion des connaissances dans les universités, les bassins hydrographiques protégeant la pratique artistique visuelle ont cédé la place à des formes d’expression multimodales.

Il s’agit notamment de projets d’archives, d’écriture créative, de communications, de films documentaires, d’essais cinématographiques, de projets de cartographie et de localisation, d’art sonore, de théâtre et de performance, de récits transmédiaux, et d’autres encore.

Le changement épistémique qu’implique la reconfiguration de ces résultats en tant que nouvelles pratiques qui complètent ou remplacent les voies traditionnelles de la production de connaissances n’a pas encore été testé en grande partie, malgré une reconnaissance initiale importante de la part des organismes de financement de la recherche, des comités de recrutement des enseignants et d’autres poches d’inventivité.

Notre symposium (hybride) d’une journée à l’université de Trent (Peterborough, Ontario, Canada) examinera ces questions en posant les problèmes suivants :

- Quelle est la frontière qui sépare les pratiques créatives des pratiques non créatives ?
- Quelles sont les approches adoptées par les artistes pour transférer leur pratique dans un milieu producteur de connaissances ?
- Quels sont les lieux de présentation des projets de recherche-création et quels sont les publics ?
- Comment la recherche-création universitaire répond-elle à la demande d’implication de la communauté et de responsabilité ? Quelles sont les garanties institutionnelles ?
- Comment la valeur artistique a-t-elle été réorientée pour répondre aux exigences du marché de l’université (distinctes de celles des industries culturelles) ?
- Comment les œuvres créatives développent-elles des voies d’accès à travers les repères établis pour assurer le financement de la recherche ? Comment les organismes de financement ont-ils réagi à ces changements et sont-ils viables ?
- Quelles sont les implications du développement de travaux sous différents parapluies conceptuels linguistiques, nationaux, régionaux ou mondiaux (par exemple, “basé sur la pratique” par rapport à “dirigé par la pratique”) ?
- Quelles sont les implications de la recherche-création pour l’enseignement de premier cycle, l’encadrement des diplômés et le mentorat ?
- Comment une forme de connaissance fondée sur la recherche-création se conforme-t-elle aux rubriques d’évaluation pour l’embauche, la réaffectation, la titularisation et la promotion ?
- Comment les productions créatives contribuent-elles à promouvoir l’équité et l’accès ?
- Dans quelle mesure la recherche-création, envisagée comme une intervention, participe-t-elle au travail en cours de décolonisation des universités ?
- Que révèle la recherche-création aux chercheurs réputés “traditionnels” sur leurs propres pratiques ? – De quoi ces chercheurs ont-ils peur lorsqu’ils rencontrent des projets de recherche-création ?
- Qu’apporte la diversification des connaissances et des méthodes aux débats historiques sur le sujet et à qui cela sert-il ?

Bien que bon nombre des questions nécessaires ont déjà été posées, le consensus n’a pas été atteint et ne devrait peut-être jamais l’être. Le contrat social en cours d’élaboration sur la recherche-création pourrait aboutir à des résultats moins souhaitables que l’état actuel des choses. Nous visons donc à construire une communauté de chercheurs liés par la solidarité plutôt que par l’unanimité et nous accueillons les dissensions dans nos rangs. Au lieu d’exiger des réponses, nous cherchons à poser de meilleures questions.

Nous invitons les chercheurs, y compris les professeurs et les étudiants diplômés, les artistes, les praticiens, les créatifs et les collaborateurs à présenter de courts “manifestes” (~ 5 minutes) qui explorent au moins un aspect de l’épistémologie émergente. Ces présentations seront organisées lors d’un événement d’une journée à l’Université Trent (Peterborough, Ontario, Canada), ainsi qu’à distance via Zoom.
