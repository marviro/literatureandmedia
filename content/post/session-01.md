---
date: 2020-10-02
dateevent: 2020-10-02
title: "1st session: Questioning Inscription"
source: https://demo.codimd.org/BRExcT9kSxu2fuBJu7CaaQ
summary: "What inscription mean."
home: yes
aliases: "/session1.html"
---
This first meeting "Questioning Inscription" will be held on October 2nd from 1pm to 2:30pm (Montreal time zone) and will be recorded (if you wish, you will be able to turn off your camera). The link of the meeting is the following:  
[https://meet.jit.si/CCLA-MediaLiteratureTheory-FirstWorkSession](https://meet.jit.si/CCLA-MediaLiteratureTheory-FirstWorkSession)

Here a draft program for this introductive working session:

0. **Group presentation and general information @monique** [see home page](https://ccla.digitaltextualities.ca) : introduction of participants / tour de table
1. **Introduction**: quick presentation of the 3 sessions
2. **About text**: present differences between full text / format like plain text; what a docx doc is really? Material text/inscription & formats aren't neutral or free, how you writes matters in the product @marcello
3. **Revisiting Representation**: IT architectures and imaginaries of text (persistence and transfer, APIs and RESTful interfaces) and their relation to representation of knowledge; what constitutes a "resource", what constitutes knowledge; affordances of interfaces to text for access, sharing and commons, and commodification; information architectures as an object of analysis and intervention; text vs. database; about [*Knowledge Design* by Jeffrey Schnapp](http://jeffreyschnapp.com/wp-content/uploads/2011/06/HH_lectures_Schnapp_01.pdf), and *[A Publisher's job is to provide a good API](http://toc.oreilly.com/2013/02/a-publishers-job-is-to-provide-a-good-api-for-books.html)* by Hugh McGuire @markus
4. **Materiality Matters**: examples of using API, toolchains/publishing chains like *Les Ateliers* @antoine
5. **Format as Knowledge Structuring and Modeling Issue**: talking about formats (semantics and graphics) + Demonstration with Stylo export (Margot)
6. **Text and data**: practical examples with zotero (Antoine)


## Interesting quotes

>The last historical act of writing may well have been the moment when, in the early seventies, the Intel engineers laid out some  dozen  square meters of blueprint paper (64 square meters in the case of the later 8086) in order to desi_gn the hardware architecture of their first integrated microprocessor.  
>[Kittler, _There is no software_](https://monoskop.org/images/f/f9/Kittler_Friedrich_1992_1997_There_Is_No_Software.pdf)

## Notes during the session

### Markus
https://demo.codimd.org/BSPDGyxDTJa7FxHLqTR0vA

### Antoine
Les ateliers de [sens public]: https://ateliers.sens-public.org/

- tools adapted with the theory of what the text should be
- challenging the logic of the institutionalization process in academia
- think about the stability of the materialities we use

### Margot
format as mode of representation of knowledge
commercial/economic dynamic comes first
but also public aspect of the digital space and need to develop new collective practices
What you see is what you mean (WYSIWYM)
What you see is what you get (WYSIWYG)

Monique:
What can we do in CompLit Canada to think about our practices?
    -Marcello: 1) how we look at texts
            2) how we write about them
            3) playing with the available tools

Lai-Tze:
Planning the next events

## Video of this first session
If you want you can download the video of the first session (~800Mo) :  
[https://owncloud.ecrituresnumeriques.ca/index.php/s/YcMzoBUI1Fn3Dl6](https://owncloud.ecrituresnumeriques.ca/index.php/s/YcMzoBUI1Fn3Dl6)

## Next events:
- workshop 2: markdown
- workshop 3: versioning

- plan to send an abstract for a session / worshop / table ronde for Congress 2021 with CCLA:
curent deadline = 15 Nov. (might be extended)
we can think about format

- example of what has been done (multilingual live translation): https://aabrahams.wordpress.com/2019/06/06/about-all-things-languages-of-all-sorts/


###### tags: `CCLA`
