---
date: 2022-02-21 
dateevent: 2022-02-22 
title: "Graph you research! / Graph ta recherche !"
source: https://demo.hedgedoc.org/p62Zlsa6TN6FOseqRJcb9g
summary: "The references that bind us: workshop to build knowledge networks from a Zotero library / Les références qui nous lient : atelier pour constituer des réseaux de connaissances à partir d'une bibliothèque Zotero. Margot Mellet, Antoine Fauchié, Arilys Jia & Marcello Vitali-Rosati"
home: yes
---
The references that bind us: workshop to build knowledge networks from a Zotero library

Les références qui nous lient : atelier pour constituer des réseaux de connaissances à partir d'une bibliothèque Zotero


Atelier donné en collaboration avec le Groupe de recherche sur les matérialités comparées de l'Association canadienne de littérature comparée mardi 22 février 2022 à 9h30 (HE) : [https://meet.jit.si/CCLA2022-AtelierCRCEN](https://meet.jit.si/CCLA2022-AtelierCRCEN)

Les références nous forment en tant que chercheurs aussi bien qu'en tant que communauté de recherche parce qu'elles représentent nos travaux ou que nous les utilisons pour fonder le cadre théorique de nos travaux. Il est donc préférable que ces dernières soient bien structurées, et des outils en ligne tels que Zotero sont des initiatives permettant d'organiser ses références en bibliothèque. Mais au-delà d'une utilisation personnelle, le principe de la bibliothèque de références peut être abordé comme un espace collectif de savoir. Dans cet atelier, nous souhaitons constituer une bibliothèque Zotero comme un espace collaboratif d'interaction et de discussion sur les références qui s'y trouvent pour afficher les liens entre les recherches et entre les chercheur·euse·s. En cliquant sur le lien de la bibliothèque créée pour l'atelier, les participant·e·s auront la possibilité de participer à la bibliothèque commune en ajoutant des références, en structurant les informations existantes, en témoignant de leurs expériences de lecture dans des notes, etc. Dans le but de jouer avec nos données de recherche, nous allons fetcher cette bibliothèque et produire en Python des visualisations des informations et des liens qui la composent pour montrer que nos références désignent bien plus que les articles ou ouvrages auxquels elles renvoient, et qu'elles participent d'un véritable réseau de connaissances.

Pour la rencontre du 22 février, nous demandons aux personnes participant à l'atelier de préparer au préalable quelques références (4-5 références) et de les ajouter à la bibliothèque Zotero participative.

L'évènement pourra être enregistré mais vous pourrez éteindre votre caméra si vous le souhaitez.