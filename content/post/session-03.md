---
date: 2020-12-15
dateevent: 2020-12-15
title: "3d session: Git and versioning"
source: https://demo.codimd.org/zLC2q0OvTAyB5haLXAIbkg
summary: "About versioning for the text and the literature."
home: yes
aliases: "/session3.html"
---
**Tuesday 15th december, 14h30-15h30 (east-time)**

Online: [https://meet.jit.si/CCLA-MediaLiteratureTheory-ThirdWorkSession](https://meet.jit.si/CCLA-MediaLiteratureTheory-ThirdWorkSession)

À la suite des discussions précédentes sur les modalités d'inscription puis sur l'écriture sémantique avec le langage de balisage léger Markdown, cette troisième session Littérature et média se concentrera sur le versionnement, et plus particulièrement sur Git.

Cette session sera découpée en 3 parties :

1. Markus Reisenleitner : aspects théoriques de la gestion de version et plus particulièrement de Git
2. Markus Reisenleitner et Antoine Fauchié : démonstration dans différents projets, y compris littéraires
3. détournements créatifs

Following previous discussions on inscription and on semantic writing with the Markdown lightweight markup language, this third Literature and Media session will focus on versioning, and especially on Git.

This session will be divided into 3 parts:

1. Markus Reisenleitner: theoretical aspects of versioning, and more specifically of Git (https://demo.hedgedoc.org/aofGIlF-RiGaFTlleA_lTA?both#)
2. Markus Reisenleitner and Antoine Fauchié: Demonstration of various projects, including literary projects
3. Creative hacks


## Notes durign the session

- conceptual differences between word and markdown 



###### tags: `CCLA`
