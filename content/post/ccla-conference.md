---
date: 2021-05-09
dateevent: 2021-05-14
title: "CCLA Annual Meeting"
source: https://demo.hedgedoc.org/bUzHlzPZQcKrIlHqBO3yYg
summary: "Le congrès de l'association canadienne de littérature comparée / Annual Meeting of the Canadian Comparative Literature Association : Changing Directions in Comparative Literature / Les nouvelles voies de la littérature comparée"
home: yes
aliases: "/ccla-conference.html"
---
**Le congrès de l'association canadienne de littérature comparée / Annual Meeting of the Canadian Comparative Literature Association : "Changing Directions in Comparative Literature" / « Les nouvelles voies de la littérature comparée »**

Tuesday 11th - Sunday 16th of May

## Panels Hosted by the Research Group Comparative Materialities: Media, Literature, Theory / groupe de recherche Matérialités comparatives: médias, littérature et théorie

### Panel III  
13rd of May
14:30 - 17:00
Workshop: *Enacting Inscription*
Moderator: Marcello Vitali Rosati
lien : https://meet.jit.si/cclaaclc2021inscriptions

Pad for this panel : [Panel III](https://demo.hedgedoc.org/tl4XcSrdQNawTGMBMTvyuQ#)

### Panel II 
14th of May
11:45 - 13:45 
Workshop: *Questionning Inscription* 
Moderators: Monique Tschofen & Lai-Tze Fan
lien : https://meet.jit.si/cclaaclc2021inscriptions


_For the organisation of the panels: [https://demo.hedgedoc.org/SKaez5wCR3SoZCfz5cJpcg](https://demo.hedgedoc.org/SKaez5wCR3SoZCfz5cJpcg)_
