---
date: 2024-01-01
dateevent: 2024-13-06 
title: "Colloque de l’Association canadienne de littérature comparée"
source: 
summary: "Colloque de l’Association canadienne de littérature comparée
Canadian Comparative Literature Association Conference - du 13 au 16 juin 2024 - Appel à communications – Call for Papers"
home: yes
--- 

To read the [PDF](https://complit.ca/wp-content/uploads/2023/10/CCLA-CFP-2024.pdf)

Le français ne suit pas. *This is a colingual CFP.*[^1]

*Thinking through what sustainability signifies for our collective social bodies and
languages means, more than ever, being attentive to the gaps opened up by the concept. As
comparatists working across and between languages, we are confronted with its
untranslatability.* Aucune « durabilité » ne parvient à rendre ce qui est soutenu par la
sustainability, mais le désir d’assurer – de faire en sorte que les choses arrivent – est significatif
de ce qu’une pensée comparatiste en mouvement peut faire émerger. *When we consider it
through a comparative lens, what is sustained and sustainable appears indeed as that
willingness and commitment to make things happen, creating the platforms, formats, spaces,
and also the gaps that nurture them.* Si, comme le propose Timothy Morton, *“The ecological
society to come, then, must be a bit haphazard, broken, lame, twisted, ironic, silly, sad”,*[^2] nous
proposons de penser notre rôle de comparatistes comme acteur·ices de ces espaces
d’incohérence générateurs d’une vision du futur qui aborde les inégalités, les étrangetés et les
impensés.

Following our 2020 manifesto, [Knowledge is a commons – Pour des savoirs en commun](https://complit.ca/2020/06/29/complit-manifesto/), this conference will think through what our comparatist approach can bring to the creation of more sustainable thinking, teaching, and research practices. Comment penser la pérennité de nos modèles de pensée en conditions précaires tout en les faisant évoluer avec
nos réalités culturelles ? Comment actualiser notre enseignement en le concevant comme
dialogue avec l’expérience et le travail intellectuel de nos étudiant·es ? How does our
comparative methodology approach sustainability in a world contemplating its own end? How
does our relationship to the canon influence what we keep (and don’t keep) of cultural
productions that cohabitate and compete on the inequitable playing field of human history?

[^1]: We borrow the term « colingual », the fair cohabitation of languages, from Catherine Leclerc.
[^2]:  Timothy Morton, All Art is Ecological, Penguin Books (Green Ideas), p.17.

En vue du colloque de l’ACLC qui aura lieu du 13 au 17 juin 2024 à l’Université
McGill dans le cadre du Congrès des sciences humaines, nous invitons les propositions de
communications en français, en anglais ou dans les deux langues qui s’inscriront dans la lignée
de ce thème. Les présentations et activités proposées peuvent prendre les formes suivantes :

- présentation de recherche individuelle ou collective ;
- présentation de recherche-création individuelle ou collective ;
- table ronde (incluant le thème de la discussion et les noms des participant·es) ;
- atelier dédié à un thème ou à une pratique ;
- autre format explorant les alternatives de la communication scientifique : les formats
alternatifs pourront être justifiés par la démarche des présentateur·ices et constituent
une exploration du thème du colloque de manière collective.

The CCLA Congress will take place from June 13 to 17 at McGill University, in
conjunction with the 2024 Congress of the Humanities and Social Sciences, and we currently
welcome submissions in French, English, or both languages, dedicated to our yearly theme.
The proposed presentations and activities can take the following forms:
- individual or collective research paper;
- individual or collective research-creation performance;
- roundtable (including the theme of the discussion and the confirmed speakers);
- workshop on a particular theme or skill;
- any other format examining alternative scientific communication: alternative formats
may be justified by the presenters’ approach and provide a collective exploration of
the conference theme.

Nous vous invitons à soumettre une proposition de 200 à 250 mots pour une présentation
de 15 à 20 minutes ou une activité de 75 minutes, incluant une mention du format proposé, en
pièce jointe, à l’organisatrice du colloque, Jeanne Mathieu-Lessard
(**aclc.ccla2024@gmail.com**) avant le **10 janvier 2024**. Please submit 200-250 word abstracts
(for a 15-20 minute paper or a 75-minute activity), including a mention of the proposed format
of your presentation, as attachments to the Program Chair, Dr. Jeanne Mathieu-Lessard
(**aclc.ccla2024@gmail.com**) by **January 10, 2024**.

Les présentateur·ices étudiant·es et en début de carrière seront invité·es à proposer leur
présentation pour le Prix Esther Cheung, et des présentations choisies tirées du colloque seront
publiées dans un numéro spécial de la *Revue canadienne de littérature comparée*. Graduate
students and early career presenters will be invited to have their presentation considered for
the Esther Cheung Award, and selected conference papers will be published in a special issue
of the *Canadian Review of Comparative Literature*.
