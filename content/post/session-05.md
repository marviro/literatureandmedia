---
date: 2021-03-28
dateevent: 2021-03-30
title: "5th session: Public Lecture: Visual Materialities / Matérialités visuelles"
source: https://demo.hedgedoc.org/tbj9SqEdQRS2zY3T9HyJng
summary: "Lai-Tze Fan & Joshua Synenko"
home: yes
---
Speakers: Lai-Tze Fan & Joshua Synenko

Mardi 30 mars 2021 à midi (HNE) / Tuesday, March 30, 2021, @12:00 noon EST

Online/En ligne: [https://meet.jit.si/CCLA_materialities_2021](https://meet.jit.si/CCLA_materialities_2021)

Taking notes/prise de note: [https://demo.hedgedoc.org/Vgtbl3heQqS7crpvKayuXg](https://demo.hedgedoc.org/Vgtbl3heQqS7crpvKayuXg)

## Le design genré des technologies, des machines à écrire aux assistantes numériques en intelligence artificielle — Lai-Tze Fan, University of Waterloo
Cette conférence présentera certains aspects de mon
projet financé par le CRSH, Unseen Hands: A Material
History of the Gendered Design of Technologies from
Typewriters to AI Virtual Assistants. Plus spécifiquement,
j’avance que les technologies qui sont associées au travail
genré – comme les machines à écrire de l’ère industrielle
et les assistantes numériques en intelligence artificielle –
sont en fait sciemment genrée dans leur design. Dans
cette présentation, j’examine une méthode pour effectuer
une ingénierie inverse du matériel des premières
machines à écrire produites en masse dans les années
1870, puis après un saut dans le temps de 150 ans, pour
effectuer une ingénierie inverse des logiciels populaires
d’assistantes virtuelles comme Alexa d’Amazon et Siri
d’Apple. La conclusion de cette présentation est la
suivante : le fait de genrer les technologies de service a
toujours été une méthode pour transformer les femmes et
leurs corps en machines exécutant un travail et des
services dits « humbles », incluant taper des textes dictés,
acheminer des appels et indiquer la direction.

## The Gendered Design of Technologies from Typewriters to AI Digital Assistants — Lai-Tze Fan, University of Waterloo
This talk will introduce aspects of my SSHRC-funded
project Unseen Hands: A Material History of the
Gendered Design of Technologies from Typewriters to
AI Virtual Assistants. Specifically, I argue that
technologies that are associated with gendered labour--
such as Industrial-era typewriters and digital-era AI
virtual assistants—are in fact gendered by design. In this
talk, I discuss a method of reverse engineering the
hardware of the first mass-produced typewriters in the
1870s, and then jump ahead 150 years, reverse
engineering the software of popular virtual assistants
such as Amazon’s “Alexa” and Apple’s “Siri.” The
takeaway of the talk is as such: the gendering of service
technologies has always been a method of abstracting
women and their bodies into machines that perform so-
called menial labour and services, including by typing
dictation, connecting calls, and giving directions.

---

## « Où s’arrête ce monde? » Espace, temps et image dans Parallel d’Harun Farocki — Joshua Synenko, Trent University
À travers une voix narrative essayistique, Parallel d’Harun
Farocki (2012) décrit les transformations matérielles qui se
produisent par les images numériques, empruntant à une
archive de paysages de jeux vidéos catégorisés par des
éléments comme le feu, l’eau, la terre et l’air. Contrairement
à Sunspring d’Oscar Sharp (2016), qui souligne les collisions
étranges résultant d’une machine à écrire des scénarios
dotée d’intelligence artificielle à forte charge émotionnelle,
Farocki pousse la construction de l’image et l’art de la
conservation de la présentation vidéo jusqu’à la limite de
territoires inexplorés. Cette présentation offre une analyse
de Parallel guidée par les questions suivantes : Quels
formats narratifs peuvent exprimer le pouvoir inhérent aux
images construites de façon numérique? Quelle méthode de
représentation réside au centre de ces formats, qu’il
s’agisse d’imitation ou de simulation? Quels sont ses
registres affectifs, et quels critères d’évaluation pouvons-
nous utiliser pour en mesurer les résultats?

## "Where Does This World End?" Space, Time and Image in Harun Farocki’s Parallel — Joshua Synenko, Trent University
Through an essayistic narrative voice, Harun Farocki’s
(2012) Parallel describes the worldly transformations that
occur through digital images, borrowing from an archive of
video game landscapes categorized by elements including
fire, water, earth, and air. Unlike Oscar Sharp’s (2016)
Sunspring, which highlights the bizarre collisions that
result from an affectively charged AI scriptwriting machine,
Farocki pushes both image construction, and the curatorial
arts of video presentation, to the uncomfortable limits of
uncharted territories. This paper offers an analysis of
Parallel through the following questions: What formats of
storytelling can express the power inherent in digitally
constructed images? What is the method of
representation that lies at the center of those formats,
whether it be mimicry or simulation? What are its affective
registers, and what evaluation criteria can we use to
assess the outcomes?


