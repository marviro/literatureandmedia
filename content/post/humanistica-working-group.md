---
date: 2021-05-03
dateevent: 2021-05-11
title: "Working Group / Groupe de travail during Humanistica"
source: https://demo.hedgedoc.org/W5M4-OO9SemoBRYFAGhY5g
summary: "Le groupe CCLA Littérature et média se réunit dans le cadre du colloque Humanistica 2021, pour faire le point sur la première année d'acitivité. CCLA Literature and Media Group meets to review the first year of activity."
home: yes

---
Le groupe CCLA Littérature et média se réunit dans le cadre du colloque Humanistica 2021, pour faire le point sur la première année d'acitivité. CCLA Literature and Media Group meets to review the first year of activity, during the Humanistica Conferences.

Tuesday 11th May 8am to 10:45am (EST)  
Mardi 11 mai 2021 de 8h à 10h45 (HNE)

Inscription gratuite mais obligatoire / Free inscription but obligatory:  
[https://humanistica2021.sciencesconf.org](https://humanistica2021.sciencesconf.org)

Taking notes/prise de note:  
[https://demo.hedgedoc.org/Vrl2XXqIR7-5LVVVW8Xx6A](https://demo.hedgedoc.org/Vrl2XXqIR7-5LVVVW8Xx6A)