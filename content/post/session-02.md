---
date: 2020-11-27
dateevent: 2020-11-27
title: "2nd session: Markdown & Semantic writing"
source: https://demo.codimd.org/laEmtIicR1CsGi6O5T0Tig
summary: "How we can write in a semantic and easy way."
home: yes
aliases: "/session2.html"
---
Friday 27th november, 14h30-15h30(east-time) 

Online: [https://meet.jit.si/CCLA-MediaLiteratureTheory-SecondWorkSession](https://meet.jit.si/CCLA-MediaLiteratureTheory-SecondWorkSession)

Dans la continuité des réflexions présentées lors de la première séance, il s'agira de questionner un format d'écriture particulier - le markdown - pour comprendre de quelle manière ce format représente et détermine un mode de pensée. L'atelier sera à la fois pratique (présentation et explication du format) - et théorique - réflexion sur les enjeux épistémologiques.

Continuing the reflections presented during the first session, the aim of this meeting is to question a particular writing format -  markdown - in order to understand how this format represents and determines a way of thinking. The workshop will be both practical (presenting and explaining the format) - and theoretical, reflecting on the epistemological issues at stake.

Detailed program:

- Margot Mellet: Introduction
- Antoine Fauchié et Margot Mellet: présentation de l'editor de texte Stylo / presentation of the text editor Stylo
- Antoine Fauchié: exercises pratiques / practical exercises
- Eugénie Matthey-Jonais: expériences d'utilisation / user experiences
- Markus: réflexions sur les formats de balisage sémantique pour l'écriture et la publication / reflecting on semantic markup formats for writing and publication: markup / html



## Notes

Création en 2004. Aaron Swartz (https://en.wikipedia.org/wiki/Aaron_Swartz) avait déjà créé une syntaxe semblable deux ans avant. [Atx](http://www.aaronsw.com/2002/atx/intro).

Md: simplification d'HTML

[Official website](https://daringfireball.net/projects/markdown/)

There is no schema as in XML, you can use the syntax as you want (as in HTML)

Markdown is a format, not a tool or a software: you can use it whereever you want.

Pandoc a conversion tool:

- [pandoc](http://pandoc.org)

Markdown editors:

- all the text editors you like... Atom, brackets, textedit, vim... whatever have markdown syntax extensions

Online:
- [hedgedoc](https://demo.hedgedoc.org/)
- [Stylo](stylo.huma-num.fr/)

An example of more structured content:

I can use a syntax like this:

This is a name of a city: `[Athens]{.city}`

This in a pandoc flavored markdown will be transformed in an HTML like that:

`<span class="city">Athens</span>`

Markus's reflections:
    1-Archiving reasons
    2-Accessibility of academic writing to the public, as a political act
    3-Status of the text, away from monolithic vision of the text

Problèmes et questions:

- ça va durer?
- comment le faire adopter?

Md a été adopté par une communauté très large - surtout les codeurs, informaticiens etc.

Un problème important: qui mantient HTML? (Car Md en est juste une simplification). C'est https://html.spec.whatwg.org/

Donc les gafam reviennent...

Margot : we are not encouraged to make choices about formats, but just to go with the status quo, so when we do make a choice that is not the standard one it is seen as a big statement, as a caprice. There should be ways to reflect upon our choices of format from the get-go.



###### tags: `CCLA`
