---
date: 2021-04-01
dateevent: 2021-04-06
title: "6th session: Public Lecture: Screening Materialities / Matérialités d'écrans"
source: https://demo.hedgedoc.org/fJMQ4tcUQGiJEYCEchD93A
summary: "Alevtina Lapiy, Dhvani Ramanujam, Marion Gruner, Brandon Petryna & Christina Anto"
home: yes
---
Speakers:

- **Alevtina Lapiy (Ryerson)** - What Kind of Thing is a Short Film Programm?
- **Dhvani Ramanujam (Ryerson)** - Reactivating the Film Festival Archive: On the History and Curatorial Practices of the In Visible Colours Festival
- **Marion Gruner (Waterloo)** - Reader Worlds: Constructing Context for Historical Readers of Western Story Magazine Google Tour Builder
- **Brandon Petryna (Waterloo)** - Animated Life Writing
- **Christina Anto (Ryerson)** - Braid and Invisible Cities

Mardi 6 avril 2021 à 13h (HNE) / Tuesday, April 6, 2021, 1pm (EST)

Online/En ligne: [https://meet.jit.si/CCLA_materialities_2021](https://meet.jit.si/CCLA_materialities_2021)

Taking notes/prise de note: [https://demo.hedgedoc.org/GbIv7-PsRS2c4L6ISOxtAw](https://demo.hedgedoc.org/GbIv7-PsRS2c4L6ISOxtAw)
