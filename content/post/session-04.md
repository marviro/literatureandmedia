---
date: 2021-02-03
dateevent: 2021-02-23
title: "4th session: Public Lecture: Visual Materialities / Matérialités visuelles"
source: https://demo.hedgedoc.org/P3otHIibSTKWkIuwltZmYw
summary: "Mehraneh Ebrahimiand & Laurence Sylvain"
home: yes
aliases: "/session4.html"
---

Speakers: Mehraneh Ebrahimiand & Laurence Sylvain

Mardi 23 février 2021 à midi (HNE) / Tuesday February 23, 2021, @12:00 noonEST

Online/En ligne: [https://meet.jit.si/CCLA_materialities_2021](https://meet.jit.si/CCLA_materialities_2021)

Our second season in the series run by the CCLA-ACLCworking group Comparative Materialities: Media, Literature, Theory will be launched with a session on visual materialities.

Notre deuxième saisonde la série d’événements organisés par le groupe de recherche Matérialités comparatives: Médias, littérature et théorie de l’ACLC débutera avec une première séance sur les matérialités visuelles. 

## Migrant Dreams Build Nations: Shirin Neshat’s Reverent Gaze in *Land of Dreams* / Les rêves des migrant·es construisent les nations: le regard respectueux de Shirin Neshat dans *Land of Dreams*

Mehraneh Ebrahimi, PhD, Assistant Professor, York University

Migrant dreams are potent political vehicles for envisioning the nation of the future. Shirin Neshat has captured the abject migrants dreamy gaze with reverence and in so doing endowed them with dignity and human rights.Her collection *Land of Dreams* distills the exilic imaginary in the desolate landscape of the present moment. A murky analogy to the American Dream, her work explores the unconscious dream life of migrants in these dark times of walls and ICE. She represents the exilic subjects with dignity. The proximity of the migrant’s face awakens a sense of responsibility. The aporetic gaze is a salutary reminder that the West is complicit in the plight of the migrant and thus response-able.

Les rêves des migrant·es sont des véhicules politiques puissants pour penser la nation de l’avenir. Shirin Neshat a su capter avec respect le regard rêveur des migrant·es, et, ce faisant, les a doté·es de dignité et de droits humains. Son recueil *Land of Dreams* distille l’imaginaire de l’exil au sein du paysage désolé du moment présent. Analogie trouble avec le rêve américain, son travail explore la vie rêvée inconsciente des migrant·es en cette période sombre de murs et d’ICE. Elle représente les sujets exilés avec dignité. La proximité des visages des migrant·es fait naître un sentimentde responsabilité. Le regard aporétique est unrappel salutaire que l’Occident est complice de la détresse des migrant·es et qu’il peut donc y répondre. 

## De la technique du tableau: Paul Klee et Walter Benjamin / The canvas as technique: Paul Klee and Walter Benjamin

Laurence Sylvain, Candidate au doctorat, Littérature comparée, Option Théorie et épistémologie, Université de Montréal

L’un des arguments les plus connus de Klee à propos de la peinture est celui de la conception de la toile comme corps, comme squelette, qu’il rend par l’expression «anatomie du tableau» (argument que l’on retrouve aujourd’hui dans le livre intitulé *Théorie de l’art moderne*, qui contient nombre des conférences qu’il a données lorsqu’il enseignait au Bauhaus). Cette conférence examinera les liens entre cette conception du tableau et l’articulation de la technique par Benjamin dans son célèbre *L’œuvre d’art à l’époque de sa reproductibilité technique*. Pour les deux penseurs, l’appareillage, c’est-à-dire la technique, informe profondément l’œuvre d’art, puisqu’elle est aussi un dispositif de la pensée. Cette conférence abordera également l’influence du baroque chez lesdeux penseurs, influence qui s’inscrit dans une telle conception de l’œuvre d’art, le baroque ouvrant la voie à un perspectivisme intimement lié à la technique en tant que dispositif de la pensée.

One of Klee’s well-known argumentsabout painting is his conception of the canvas as a body, as a skeleton, which he renders by the expression “anatomy of the canvas” (an argument which can now be found in the book titled *On Modern Art*, which contains many of the lectures he presented while teaching at the Bauhaus). This conference will discuss how this conception of the canvas is related to Benjamin’s articulation of technique in his famous *The Work of Art in the Age of its Technological Reproducibility*. For both thinkers, the “apparatus,” i.e., the technique, informs the work of art in a profound way, for it is also a dispositive of thought. This conference will also discuss the influence of the Baroque onboth thinkers, since it is part of such a conception of the work of art, the Baroque paving the way for a perspectivism that is intimately linked to the technique as a dispositive of thought.

## Recording

<iframe src="https://archive.org/embed/ccla-materialities-2021" width="640" height="480" frameborder="0" webkitallowfullscreen="true" mozallowfullscreen="true" allowfullscreen></iframe>

