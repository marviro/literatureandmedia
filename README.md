## About the CCLA Work Session
Thinking about literature leads us to question its material inscription and the relationship between media that informs our interpretation of the world. This research group will look at the interconnection between our ways of thinking and our discourses, and their material support, and will discuss how a comparative approach can shed light on our relationship with media.

Draft website: [ccla.ecrituresnumeriques.ca](https://ccla.ecrituresnumeriques.ca)

## About the publication chain
This repository is a minimal publication chain that uses Codi pads for building several HTML pages.

**Please modify the Codi pads but not the Markdown files.**

_[Modify the source](https://demo.codimd.org/gawCnlQyQla4ctWNLpkR9A)_

###### tags: `CCLA`